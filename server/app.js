const express = require("express");
const config_data = require("./config/config.js");
const path = require("path");
const db = require("./models/db_index");
const sendMail = require("./mailer/mailer");
const Tutorial = db.tutorials;

db.sequelize.sync();

const app = express();

const port = config_data.PORT;

app.use(express.static(path.join(__dirname, "../public")));

app.use(
  express.urlencoded({
    extended: true,
  })
);

app.get("/users", async (req, res) => {
  try {
    await Tutorial.findAll({
      order: [[db.Sequelize.col("top_score"), "asc"]],
      limit: 5,
    }).then((data) => {
      res.json(data);
    });
  } catch (err) {
    res.status(500).send({
      message: err.message || "Some error occurred while retrieving data.",
    });
  }
});

app.post("/send", async (req, res) => {
  try {
    if (!req.body.user_name || !req.body.email) {
      res.status(400).send({
        message: "Content can not be empty!",
      });
      return;
    } else {
      Tutorial.findAll({ where: { email: `${req.body.email}` } })
        .then((data) => {
          const tutorial = {
            email: req.body.email,
            name: req.body.user_name,
            top_score: req.body.high_score,
          };
          if (data.length == 0) {
            Tutorial.create(tutorial)
              .then((data) => {
                res.redirect("/");
              })
              .catch((err) => {
                res.status(500).send({
                  message:
                    err.message ||
                    "Some error occurred while creating the data.",
                });
              });
          } else {
            ///commond for updation
            // res.send(data);
            if (data[0].dataValues.top_score > req.body.high_score) {
              Tutorial.update(tutorial, {
                where: { email: req.body.email },
              })
                .then((num) => {
                  if (num == 1) {
                    res.redirect("/");
                  } else {
                    res.send({
                      message: `cannot update`,
                    });
                  }
                })
                .catch((err) => {
                  res.status(500).send({
                    message: "Error updating",
                  });
                });
            }
          }
        })
        .catch((err) => {
          res.status(500).send({
            message: err.message || "Some error occurred while retrieving data",
          });
        });

      sendMail(req, res);
    }
  } catch (err) {
    response.status(500).json({ message: "Internal Server Error" }).end();
  }
});

app.listen(port, () => {
  console.log(`listening to port ${port}.....`);
});
