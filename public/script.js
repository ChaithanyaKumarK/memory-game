const gameContainer = document.getElementById("game");
let noOfClicks = 0;

window.addEventListener("load", () => {
  //setting up local storage
  document.getElementById("bestScore_game_value").innerText = "0";
  window.localStorage.setItem("bestScore", "9999");

  //setting default error message
  document
    .getElementById("input_email")
    .setCustomValidity("email should not be empty");
  document
    .getElementById("input_user_name")
    .setCustomValidity("username should not be empty");

  //setting data
  let rank = 1;
  fetch("/users")
    .then((data) => data.json())
    .then((data) => {
      data.forEach((user) => {
        const rank_column = document.getElementById("rank");
        const name_column = document.getElementById("player_name");
        const score_column = document.getElementById("high_score");
        const date_column = document.getElementById("scored_date");
        let rank_span = document.createElement("span");
        let name_span = document.createElement("span");
        let score_span = document.createElement("span");
        let date_span = document.createElement("span");
        rank_span.innerText = rank++;
        name_span.innerText = user.name;
        score_span.innerText = user.top_score;
        date_span.innerText = user.updatedAt.substring(0, 10);
        rank_column.append(rank_span);
        name_column.append(name_span);
        score_column.append(score_span);
        date_column.append(date_span);
      });
    });

  //removing loading page after page load
  const load_div = document.getElementById("load");
  load_div.style.display = "none";
});

//reset button in the game page
const reset_btn = document.getElementById("btn");
reset_btn.addEventListener("click", () => {
  let class_list_btn = document.getElementsByClassName("restart_button");
  if (noOfClicks > 0) {
    gameRestart();
  }
});

//reset button in pop_up
const pop_up_reset_btn = document.getElementById("pop_up_play_again");
pop_up_reset_btn.addEventListener("click", () => {
  gameRestart();
});

//setting icon handling
document.getElementById("setting_icon").addEventListener("click", () => {
  document.getElementById("top_score_id").classList.toggle("show");
});

const GIFS = [
  "gifs/1.gif",
  "gifs/1.gif",
  "gifs/2.gif",
  "gifs/2.gif",
  "gifs/3.gif",
  "gifs/3.gif",
  "gifs/4.gif",
  "gifs/4.gif",
  "gifs/5.gif",
  "gifs/5.gif",
  "gifs/6.gif",
  "gifs/6.gif",
  "gifs/7.gif",
  "gifs/7.gif",
  "gifs/8.gif",
  "gifs/8.gif",
  "gifs/9.gif",
  "gifs/9.gif",
  "gifs/10.gif",
  "gifs/10.gif",
  "gifs/11.gif",
  "gifs/11.gif",
  "gifs/12.gif",
  "gifs/12.gif",
];

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

let shuffledGifs = shuffle(GIFS);

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForColors(gifsArray) {
  for (let gifs of gifsArray) {
    // create a new div
    const newDiv = document.createElement("div");

    // give it a class attribute for the value we are looping over
    newDiv.classList.add(gifs);

    //creating two divs frount and back for flip action and appending to newDiv
    const front = document.createElement("div");
    front.classList.add("front");

    newDiv.append(front);

    const back = document.createElement("div");
    back.classList.add("back");
    const back_img = document.createElement("img");

    // adding image to back div
    back_img.src = gifs;
    back.append(back_img);
    newDiv.append(back);

    // call a function handleCardClick when a div is clicked on
    front.addEventListener("click", handleCardClick);

    // append the div to the element with an id of game
    gameContainer.append(newDiv);
  }
}

let card_count = 0;
let wincount = 0;
let oldEvent = null;
// TODO: Implement this function!
function handleCardClick(event) {
  // you can use event.target to see which element was clicked
  if (card_count < 2) {
    //allows only two cards at a time
    event.target.parentElement.classList.add("turn");
    card_count++;

    if (card_count === 1) {
      //allows when first card is selected
      oldEvent = event;

      incrementClicks();
    } else {
      //allows when second card is selected
      if (event.target === oldEvent.target) {
        //allows only when same card is clicked again
        card_count = 1;
      } else if (
        event.target.parentElement.classList[0] ===
        oldEvent.target.parentElement.classList[0]
      ) {
        incrementClicks();
        //allows when different card of same color is clicked
        card_count = 0;
        oldEvent = null;
        wincount++;
        if (wincount == 12) {
          //check weather the game is won or not
          if (noOfClicks < +window.localStorage.getItem("bestScore")) {
            //updates the best score to browsers local storage
            window.localStorage.setItem("bestScore", noOfClicks);
            //updating the best score in UI
            document.getElementById(
              "bestScore_game_value"
            ).innerText = noOfClicks;
          }

          setTimeout(() => {
            //win senario
            document.getElementById(
              "pop_up_best_score"
            ).innerText = `BEST SCORE :  ${window.localStorage.getItem(
              "bestScore"
            )}`;
            document.getElementById(
              "pop_up_current_score"
            ).innerText = `CURRENT SCORE : ${noOfClicks}`;
            document.getElementById(
              "input_user_score"
            ).value = +window.localStorage.getItem("bestScore");
            document.getElementById("pop_up").style.display = "flex";
          }, 100);
        }
      } else {
        incrementClicks();
        // allows when two different card is selected
        setTimeout(() => {
          event.target.parentElement.classList = `${event.target.parentElement.classList[0]}`;
          oldEvent.target.parentElement.classList = `${oldEvent.target.parentElement.classList[0]}`;
          card_count = 0;
          oldEvent = null;
        }, 1000);
      }
    }
  }
}
//event.target.classList[0]

let submit_email_flag = false;
let submit_name_flag = false;

function user_validation() {
  const name = document.getElementById("input_user_name");
  if (/[-!@#$%^&*()_+|~=`{}\[\]:";'<>?,.\/]/.test(name.value)) {
    name.setCustomValidity("No special characters!");
    name.className = "error";
    submit_name_flag = false;
  } else if (name.value == "") {
    name.setCustomValidity("username should not be empty");
    name.className = "error";
    submit_name_flag = false;
  } else {
    name.setCustomValidity("");
    name.className = "success";
    submit_name_flag = true;
  }
}

function email_validation() {
  const email = document.getElementById("input_email");
  if (email.value == "") {
    email.setCustomValidity("email should not be empty");
    email.className = "error";
    submit_email_flag = false;
  } else if (
    !/^([a-z\d\.-]+)@([a-z\d-]+)\.([a-z]{2,8})(\.[a-z]{2,8})?$/.test(
      email.value
    )
  ) {
    email.setCustomValidity("enter a valid email");
    // email.reportValidity();
    email.className = "error";
    submit_email_flag = false;
  } else {
    email.setCustomValidity("");
    email.className = "success";
    submit_email_flag = true;
  }
}

document
  .getElementById("form_send_user_details")
  .addEventListener("input", () => {
    if (submit_email_flag && submit_name_flag) {
      document.getElementById("info_submit_button").classList = "enabled";
    } else {
      document.getElementById("info_submit_button").classList = "disabled";
    }
  });

document.getElementById("setting_icon").addEventListener("click", () => {
  //click events here
});

function incrementClicks() {
  noOfClicks++;
  document.getElementById("current_score_value").innerText = noOfClicks;
  if (noOfClicks > 0) {
    document.getElementById("btn").classList.add("active");
  }
}

function gameRestart() {
  //restart logic
  card_count = 0;
  wincount = 0;
  oldEvent = null;
  noOfClicks = 0;
  document.getElementById("current_score_value").innerText = "0";
  document.getElementById("pop_up").style.display = "none";
  document.getElementById("btn").classList.toggle("active");
  gameContainer.innerHTML = "";
  let shuffledGifs = shuffle(GIFS);
  createDivsForColors(shuffledGifs);
}

// when the DOM loads
createDivsForColors(shuffledGifs);
