window.addEventListener("load", () => {
  //setting up local storage
  let local_score = window.localStorage.getItem("bestScore");
  // if (local_score == null || local_score === "0" || local_score === "9999") {
  //   document.getElementById("bestScore_value").innerText = "0";
  //   window.localStorage.setItem("bestScore", "9999");
  // } else {
  //   document.getElementById("bestScore_value").innerText = local_score;
  // }
  let rank = 1;
  fetch("/users")
    .then((data) => data.json())
    .then((data) => {
      data.forEach((user) => {
        const rank_column = document.getElementById("rank");
        const name_column = document.getElementById("player_name");
        const score_column = document.getElementById("high_score");
        const date_column = document.getElementById("scored_date");
        let rank_span = document.createElement("span");
        let name_span = document.createElement("span");
        let score_span = document.createElement("span");
        let date_span = document.createElement("span");
        rank_span.innerText = rank++;
        name_span.innerText = user.name;
        score_span.innerText = user.top_score;
        date_span.innerText = user.updatedAt.substring(0, 10);
        rank_column.append(rank_span);
        name_column.append(name_span);
        score_column.append(score_span);
        date_column.append(date_span);
      });
    });

  //removing loading page after page load
  const load_div = document.getElementById("load");
  load_div.style.display = "none";
});

document.getElementById("setting_icon").addEventListener("click", () => {
  document.getElementById("top_score_id").classList.toggle("show");
});
